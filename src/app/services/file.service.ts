import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FileService {

  private currentChatSrc = new BehaviorSubject<string>('');
  private fileSrc = new BehaviorSubject<File>(undefined);

  constructor() {
  }

  // The entire File
  getFile(): Observable<File> {
    return this.fileSrc.asObservable();
  }

  // The chat as a single long string
  getCurrentChat(): Observable<string> {
    return this.currentChatSrc.asObservable();
  }

  setCurrentChat(chat: string) {
    this.currentChatSrc.next(chat);
  }

  setFile(file: File) {
    this.fileSrc.next(file);
  }

  clearChat() {
    this.currentChatSrc.next('');
  }

  clearFile() {
    this.fileSrc.next(undefined);
  }

  removeAll() {
    this.clearChat();
    this.clearFile();
  }
}
