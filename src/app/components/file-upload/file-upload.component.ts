import {Component, ElementRef, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {ChartsService} from '../../services/charts.service';
import {FileService} from '../../services/file.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss']
})
export class FileUploadComponent implements OnInit, OnDestroy {

  @ViewChild('fileInput', {static: true}) fileInput: ElementRef<HTMLInputElement>;

  file: File;
  fileSubscription: Subscription;

  constructor(private fileService: FileService) {
  }

  ngOnInit() {
    this.fileSubscription = this.fileService.getFile().subscribe(file => {
      this.file = file;
    });
  }

  ngOnDestroy(): void {
    this.fileSubscription.unsubscribe();
  }

  fileChange() {
    const file = this.fileInput.nativeElement.files[0];
    this.fileService.setFile(file);
    const reader = new FileReader();
    reader.onload = () => {
      this.fileService.setCurrentChat(reader.result as string);
    };
    reader.readAsText(file);
  }

  remove() {
    this.fileService.removeAll();
    this.fileInput.nativeElement.value = '';
  }

}
